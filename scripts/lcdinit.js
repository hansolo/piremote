var Lcd = Packages.eu.hansolo.enzo.lcd.Lcd;

var obj = new Object();

obj.initLcd = function(lcd) {
    lcd.title          = 'Temperature';
    lcd.unit           = '°F';
    lcd.minValue       = 32;
    lcd.maxValue       = 212;
    lcd.value          = 32;
    lcd.decimals       = 1;
    lcd.lowerRightText = 'imperial';
    lcd.styleClass.setAll('lcd', 'lcd-green-darkgreen');
}
