package pi;

import eu.hansolo.enzo.lcd.Lcd;
import eu.hansolo.enzo.lcd.LcdBuilder;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;


public class Main extends Application {
    // Nashorn
    private static final String INIT_SCRIPT_FILE_NAME   = "lcdinit.js";
    private static final String CONFIG_SCRIPT_FILE_NAME = "lcdconfig.js";
    
    private ScriptEngineManager manager;
    private ScriptEngine        engine;
    private String              script;
    private Invocable           inv;
    private Object              scriptObject;
    private FileWatcher         fileWatcher;
    private Thread              dirWatcherThread;
    private String              pathToScriptFiles;

    // JavaFX Control
    private Lcd                 lcd;

    // PiProperties
    private Properties          piProperties;

    // XMPP configuration
    private String              id;
    private String              senderName;
    private String              senderPassword;
    private String              server;
    private String              resource;
    private int                 port;
    private XmppManager         xmppManager;


    // ******************* Initialization *************************************
    @Override public void init() {
        // Init Nashorn
        manager = new ScriptEngineManager();
        engine  = manager.getEngineByName("nashorn");

        // Init lcd control
        lcd = LcdBuilder.create()
                        .prefSize(1000, 438)
                        .animationDurationInMs(2000)
                        .thresholdVisible(true)
                        .minMeasuredValueVisible(true)
                        .maxMeasuredValueVisible(true)
                        .lowerRightTextVisible(true)
                        .titleVisible(true)
                        .formerValueVisible(true)
                        .decimals(2)
                        .unitVisible(true)
                        .valueFont(Lcd.LcdFont.LCD)                        
                        .styleClass(Lcd.STYLE_CLASS_STANDARD_GREEN)
                        .build();

        // Read individual settings for the pi
        piProperties      = readPiProperties();
        id                = piProperties.containsKey("id") ? piProperties.getProperty("id") : "SOME ID";
        senderName        = piProperties.containsKey("sender_name") ? piProperties.getProperty("sender_name") : "NAME OF PI";
        senderPassword    = piProperties.containsKey("sender_password") ? piProperties.getProperty("sender_password") : "PASSWORD";
        server            = piProperties.containsKey("sender_server") ? piProperties.getProperty("sender_server") : "XMPP SERVER";
        resource          = piProperties.containsKey("sender_resource") ? piProperties.getProperty("sender_resource") : "RESOURCE";
        port              = piProperties.containsKey("port") ? Integer.parseInt(piProperties.getProperty("port")) : 5222;
        pathToScriptFiles = piProperties.containsKey("path_to_scripts") ? piProperties.getProperty("path_to_scripts") : "/home/pi/piremote/";

        // Init xmpp connection
        xmppManager = new XmppManager(this, server, resource, port);

        initXmppConnection();
        initLcd();
        initFileWatcher();
    }

    private void initXmppConnection() {
        try {
            xmppManager.init();
            xmppManager.login(senderName, senderPassword);
            xmppManager.setStatus(true, "Pi("+ id +") is online");
        } catch (XMPPException exception) {
            System.out.println("Error connecting to XMPP: XMPPException " + exception);
        }
    }

    private void initLcd() {
        script = getScriptContent(INIT_SCRIPT_FILE_NAME);        
        try {
            if (null != script) {
                engine.eval(script);
                inv = (Invocable) engine;
                scriptObject = engine.get("obj");
            }
        } catch(ScriptException exception) {
            script = "";
        }
        if (null == script || script.isEmpty()) {
            // Initialize the Lcd control with default values if no script is available
            setLcdDefaults();
        } else {
            // Initialize the Lcd control with the values from the script file
            try {                                
                inv.invokeMethod(scriptObject, "initLcd", lcd);                
            } catch(ScriptException | NoSuchMethodException exception) {
                setLcdDefaults();
            }
        }
    }    

    private void initFileWatcher() {
        // Attach file fileWatcher
        if (null != dirWatcherThread && dirWatcherThread.isAlive()) dirWatcherThread.interrupt();
        File fileToWatch = new File(pathToScriptFiles + CONFIG_SCRIPT_FILE_NAME);
        fileWatcher = new FileWatcher(fileToWatch);
        fileWatcher.setOnFileModified(watcherEvent -> Platform.runLater(() -> applyConfigScript()));
        fileWatcher.setOnFileRemoved(watcherEvent -> dirWatcherThread.interrupt());
        dirWatcherThread = new Thread(fileWatcher);
        dirWatcherThread.start();
    }


    // ******************* Methods that answers requests **********************    
    public void answerScriptRequest(final String SCRIPT, final String JID) {
        new Thread(() -> {
            boolean success = false;
            try {
                engine.eval(SCRIPT);
                inv          = (Invocable) engine;
                scriptObject = engine.get("obj");
                Platform.runLater(() -> {
                    try {
                        inv.invokeMethod(scriptObject, "configLcd", lcd);
                    } catch (ScriptException | NoSuchMethodException exception) {}
                });
                success = true;
            } catch(ScriptException exception) {
                //Platform.runLater(() -> setLcdDefaults());
            }

            try {
                Message message = new Message();
                message.setBody(success ? "Script executed successfully" : "Error executing script");
                xmppManager.sendMessage(message, JID);
            } catch (XMPPException exception) {}

        }).start();
    }    

    // ******************* Methods that just execute requests *****************    
    public void shutdownRequest() {
        try {
            Runtime run = Runtime.getRuntime();
            run.exec("sudo shutdown -h now");
        } catch (IOException exception) {}
    }
    public void exitRequest() {
        System.exit(0);
    }


    // ******************* Private Methods ************************************
    private Properties readPiProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("pi.properties"));
            if (properties.isEmpty()) createPiProperties(properties);
        } catch (IOException exception) {
            createPiProperties(properties);
        }
        return properties;
    }
    private void createPiProperties(Properties properties) {
        properties.put("id", "SOME_ID");
        properties.put("sender_name", "NAME OF PI");
        properties.put("sender_password", "PASSWORD");
        properties.put("sender_server", "XMPP SERVER");
        properties.put("sender_resource", "RESOURCE");
        properties.put("port", "5222");
        properties.put("path_to_scripts", "/home/pi/piremote/");
    }
    
    private void setLcdDefaults() {
        lcd.setTitle("Temperatur");
        lcd.setUnit("°C");                
        lcd.setMinValue(0);
        lcd.setMaxValue(100);
        lcd.setLowerRightText("metric");
    }

    private void applyConfigScript() {
        script = getScriptContent(CONFIG_SCRIPT_FILE_NAME);
        try {
            engine.eval(script);
            inv          = (Invocable) engine;
            scriptObject = engine.get("obj");
        } catch(ScriptException exception) {}

        if (script.isEmpty()) { return; }

        try {
            inv.invokeMethod(scriptObject, "configLcd", lcd);
        } catch(ScriptException | NoSuchMethodException exception) {
            System.out.println("Error executing lcdconfig.js");
        }
    }   
    
    private String getScriptContent(final String SCRIPT_FILE_NAME) {
        StringBuilder scriptContent = new StringBuilder();
        try {
            Path path          = Paths.get(pathToScriptFiles + SCRIPT_FILE_NAME);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            lines.forEach(line -> scriptContent.append(line));
            System.out.println("Script: " + SCRIPT_FILE_NAME + " loaded");
        } catch (IOException  exception) {
            System.out.println("Error loading " + SCRIPT_FILE_NAME + " file");
        }
        return scriptContent.toString();    
    }


    // ******************* Application related ********************************
    @Override public void start(Stage stage) {
        StackPane pane = new StackPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.getChildren().add(lcd);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {
        xmppManager.destroy();        
    }

    public static void main(String[] args) {
        launch(args);
    }
}
