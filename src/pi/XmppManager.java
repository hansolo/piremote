package pi;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;


public class XmppManager {
    private static final int        PACKET_REPLY_TIMEOUT = 5000; // millis
    private Main                    main;
    private String                  server;
    private String                  resource;
    private int                     port;
    private ConnectionConfiguration config;
    private XMPPConnection          connection;
    private ChatManager             chatManager;
    private PacketListener          packetListener;


    // ******************* Constructors ***************************************
    public XmppManager(final Main MAIN, final String SERVER, final String RESOURCE, final int PORT) {
        main     = MAIN;
        server   = SERVER;
        resource = RESOURCE;
        port     = PORT;
    }


    // ******************* Initialization *************************************
    public void init() throws XMPPException {
        SmackConfiguration.setPacketReplyTimeout(PACKET_REPLY_TIMEOUT);

        config = new ConnectionConfiguration(server, port);

        SASLAuthentication.supportSASLMechanism("PLAIN");        
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
        config.setSASLAuthenticationEnabled(true);
        //config.setReconnectionAllowed(true);
        //config.setRosterLoadedAtLogin(true);
        //config.setSendPresence(false);
        //config.setCompressionEnabled(true);

        connection = new XMPPConnection(config);
        connection.connect();

        chatManager    = connection.getChatManager();
        packetListener = new XmppPacketListener();

        System.out.println("Connected: " + connection.isConnected());
    }


    // ******************* Methods ********************************************
    public void login(final String USERNAME, final String PASSWORD) throws XMPPException {
        if (connection != null && connection.isConnected()) {
            connection.login(USERNAME, PASSWORD, resource);            
            connection.getRoster().setSubscriptionMode(Roster.SubscriptionMode.accept_all);
            connection.addPacketListener(packetListener, new PacketTypeFilter(Message.class));
        } else {
            System.out.println("XMPP login failed...!");
        }
    }

    public void setStatus(final boolean AVAILABLE, final String STATUS) {
        Presence.Type type = AVAILABLE ? Presence.Type.available : Presence.Type.unavailable;
        Presence.Mode mode = AVAILABLE ? Presence.Mode.available : Presence.Mode.xa;
        Presence presence  = new Presence(type);
        presence.setFrom(connection.getUser());
        presence.setMode(mode);
        presence.setStatus(STATUS);
        presence.setPriority(0);
        sendPacket(presence);
    }

    public void sendPacket(Packet packet) {
        if (connection != null && connection.isAuthenticated()) {
            connection.sendPacket(packet);
        }
    }

    public boolean isConnected() {
        return connection.isConnected();
    }

    public void destroy() {
        if (null == connection) return;
        if (connection.isConnected()) {
            connection.disconnect();
        }
    }

    public void sendMessage(Message message, String receiverJID) throws XMPPException {
        if (connection.isConnected()) {
            Chat chat = chatManager.createChat(receiverJID, null);
            chat.sendMessage(message);
        }
    }


    // ******************* Inner classes **************************************
    private class XmppPacketListener implements PacketListener {
        @Override public void processPacket(final Packet PACKET) {
            final String BODY      = ((Message) PACKET).getBody().trim();
            final String FROM      = ((Message) PACKET).getFrom();            
            if (BODY.startsWith("script:")) {
                main.answerScriptRequest(BODY.substring(7), FROM);
            } else if (BODY.equals("shutdown")) {
                main.shutdownRequest();
            } else if (BODY.equals("exit")) {
                main.exitRequest();
            }
        }
    }
}
